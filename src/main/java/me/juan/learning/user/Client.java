package me.juan.learning.user;

import lombok.Data;

import java.util.HashMap;

@Data
public class Client implements IUser {
    private static HashMap<String, Client> clients = new HashMap<>();

    private String name;
    private String lastName;

    public Client(String name, String lastName) {
        this.name = name;
        this.lastName = lastName;
        clients.put(this.getFullName(), this);
    }

    public static Client getClient(String name, String lastName) {
        return clients.computeIfAbsent(name + " " + lastName, k -> new Client(name, lastName));
    }
}
