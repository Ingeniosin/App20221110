package me.juan.learning.user;

public interface IUser {

    String getName();

    String getLastName();

    default String getFullName() {
        return getName() + " " + getLastName();
    }

}
