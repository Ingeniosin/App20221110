package me.juan.learning;

import me.juan.learning.food.Ingredient;
import me.juan.learning.user.IUser;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class FoodManager {
    private static FoodManager instance;

    public static FoodManager getInstance() {
        if (instance == null) {
            instance = new FoodManager();
        }
        return instance;
    }

    private final HashMap<String, List<Ingredient>> foodByUser = new HashMap<>();

    public List<Ingredient> getFood(String clientName) {
        return foodByUser.getOrDefault(clientName, new ArrayList<>());
    }

    public void addFood(IUser user, Ingredient ingredient) {
        List<Ingredient> currentFood = getFood(user);
        currentFood.add(ingredient);
        foodByUser.putIfAbsent(user.getFullName(), currentFood);
    }

    public List<Ingredient> getFood(IUser user) {
        return getFood(user.getFullName());
    }

    public void printFood(IUser user) {
        System.out.println("==================================================");
        System.out.println("\t\tComida de " + user.getFullName());
        System.out.println("==================================================");
        System.out.println(String.join("\n\n", getFood(user).stream().map(Ingredient::toString).toArray(String[]::new)));
        System.out.println("==================================================");
    }

}
