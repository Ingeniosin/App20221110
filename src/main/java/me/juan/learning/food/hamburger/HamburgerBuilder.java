package me.juan.learning.food.hamburger;

import me.juan.learning.food.GenericIngredient;
import me.juan.learning.food.Ingredient;

import java.util.Objects;

public class HamburgerBuilder {

    private final Hamburger hamburger;
    private Ingredient lastChild;

    public HamburgerBuilder(HamburgerType hamburgerType) {
        this.hamburger = new Hamburger(hamburgerType);
        addIngredient(new GenericIngredient("Pan"));
    }

    public HamburgerBuilder addIngredient(String name) {
        return addIngredient(new GenericIngredient(name));
    }

    public HamburgerBuilder addIngredients(String... names) {
        for (String name : names) {
            addIngredient(name);
        }
        return this;
    }

    public HamburgerBuilder addIngredient(Ingredient ingredient) {
        Objects.requireNonNullElse(lastChild, hamburger).setIngredientChild(ingredient);

        lastChild = ingredient;
        return this;
    }

    public HamburgerBuilder addIngredients(Ingredient... ingredients) {
        for (Ingredient ingredient : ingredients) {
            addIngredient(ingredient);
        }
        return this;
    }

    public Hamburger build() {
        addIngredient(new GenericIngredient("Pan"));
        return this.hamburger;
    }


}
