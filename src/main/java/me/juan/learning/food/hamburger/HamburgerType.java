package me.juan.learning.food.hamburger;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum HamburgerType {

    CLASSIC("Clásica"),
    CHEESE("Con queso"),
    BACON("Con tocino"),
    BACON_CHEESE("Con tocino y queso");

    private final String name;
}
