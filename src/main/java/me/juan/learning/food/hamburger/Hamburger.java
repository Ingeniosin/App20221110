package me.juan.learning.food.hamburger;

import me.juan.learning.food.Ingredient;


public class Hamburger extends Ingredient {
    private final HamburgerType type;

    protected Hamburger(HamburgerType type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return " * Hamburguesa de tipo [ " + type + " ] \n\t - Ingredientes: " + super.toString() + "";
    }
}
