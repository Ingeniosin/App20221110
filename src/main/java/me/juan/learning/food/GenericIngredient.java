package me.juan.learning.food;

public class GenericIngredient extends Ingredient {

    public GenericIngredient(String name) {
        super(name);
    }

    public GenericIngredient(String name, Ingredient ingredientChild) {
        super(name, ingredientChild);
    }

}
