package me.juan.learning.food;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public abstract class Ingredient {

    private String name;
    private Ingredient ingredientChild;

    public Ingredient(String name) {
        this.name = name;
    }

    public Ingredient() {
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();

        if (name != null) {
            result.append(name);
        }

        if (ingredientChild != null) {
            if (result.length() > 0) {
                result.append(", ");
            }
            result.append(ingredientChild);
        }

        return result.toString();
    }
}
