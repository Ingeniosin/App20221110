package me.juan.learning;

import me.juan.learning.food.hamburger.Hamburger;
import me.juan.learning.food.hamburger.HamburgerBuilder;
import me.juan.learning.food.hamburger.HamburgerType;
import me.juan.learning.user.Client;
import me.juan.learning.user.IUser;

public class Main {
    public static void main(String[] args) {
        FoodManager foodManager = FoodManager.getInstance();
        IUser client = new Client("Juan", "Gonzalez");

        Hamburger hamburger = new HamburgerBuilder(HamburgerType.CHEESE)
                .addIngredient("Queso")
                .addIngredient("Lechuga")
                .addIngredient("Tomate")
                .addIngredient("Carne")
                .build();

        foodManager.addFood(client, hamburger);
        foodManager.printFood(client);
    }
}
